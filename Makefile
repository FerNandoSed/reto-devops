SHELL=/bin/bash

all: retodevops

retodevops: test_reto6

puerto_reto1 = 3001
puerto_reto4 = 3004

test_reto6: reto6
	echo "Ejecutando test_reto6"
	kubectl get roles -n retodevops
	kubectl get rolebindings -n retodevops
	kubectl get pods --context=auditor-context
	-kubectl delete deployment retodevops --context=auditor-context
	touch test_reto6

reto6: test_reto5
	echo "Ejecutando reto6"
	cd reto_6; bash generate_user.sh auditor
	cd reto_6; terraform init
	cd reto_6; terraform apply -auto-approve
	sleep 10
	touch reto6

test_reto5: reto5
	echo "Ejecutando test_reto5"
	bash reto_5/tests.sh
	touch test_reto5

reto5: test_reto4
	echo "Ejecutando reto5"
	bash reto_5/deploy.sh
	touch reto5

test_reto4: reto4
	echo "Ejecutando test_reto4"
	curl localhost:${puerto_reto4} -i
	curl localhost:${puerto_reto4}/private -i
	kubectl get hpa -n retodevops
	touch test_reto4

reto4: reto3
	echo "Ejecutando reto4"
	bash reto_4/deploy.sh
	touch reto4

reto3: test_reto2
	echo ejecutando reto 3 >> reto_3/.pipelinelauncher
	git commit -m "Gatilla ejecución de pipeline" reto_3/.pipelinelauncher
	git push
	touch reto3

test_reto2: reto2
	echo "Ejecutando test_reto2"
	sleep 5
	curl http://localhost -i
	curl https://localhost/public -ki
	curl https://localhost/private -ki
	curl https://localhost/private -uadmin:passwordmuys3cr3t0 -ki
	touch reto2 test_reto2

reto2: test_reto1
	echo "Ejecutando reto2"
	cp reto_2/.env_example reto_2/.env
	docker-compose up -d

test_reto1: reto1
	echo "Ejecutando test_reto1"
	sleep 5
	curl localhost:${puerto_reto1} -i
	curl localhost:${puerto_reto1}/public -i
	curl localhost:${puerto_reto1}/private -i
	touch reto1 test_reto1

reto1:
	echo "Ejecutando Reto 1"
	docker build . --tag app-retodevops
	docker run -d --name retodevops --rm --init -p${puerto_reto1}:3000 app-retodevops

clean:
	echo "Limpieza"
	docker stop retodevops
	docker-compose down
	cd reto_6; terraform destroy -auto-approve
	rm reto_6/rbac/auditor*
	kubectl delete csr auditor-csr
	helm uninstall prx-reverso -n retodevops
	minikube status && minikube stop
	rm *reto1 *reto2 *reto4 *reto5 *reto6 *reto3 terraform.tfstate
