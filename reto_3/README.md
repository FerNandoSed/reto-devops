# Reto 3: Pipeline
Opté por utilizar CI/CD de gitlab, ya que me parece más fácil de abordar (no tengo que hacer uso de servicios externos). Las etapas del pipeline son la siguientes:
  - **test**
  - **build**
  - **release**

## Test
Se utiliza misma imagen que para la construcción del contenedor de aplicación y se aprovecha las pruebas ya confeccionadas. Esta tarea se gatilla con cada evento del repositorio.

```yaml
test_app:
  image: alpine:3.14.2
  cache:
    paths:
      - node_modules
  script:
    - apk add npm nodejs
    - npm install
    - npm run test
```

## Build
Se configuran 3 trabajos, los cuales ejecutan las siguientes tareas:
  - **build_app**: Construcción de la imagen de la app nodejs y publicación de esta en el registro del repositorio
  - **build_nginx**: Contrucción/publicación de imagen de proxy reverso, utilizada por kubernetes en reto 4. Solo se gatilla si hay cambios en los archivos relacionados a la construcción de la imagen.
  - **build_helm**: Construcción de paquete para distribución de helm chart de reto 5. Solo se gatilla cuando hay cambios en el helm chart  (directorio `reto_5/proxy-reverso-retodevops`).

```yaml
build_app:
  image: docker:19.03.12
  stage: build
  cache:
    paths:
      - node_modules
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

build_nginx:
  image: docker:19.03.12
  rules:
    - changes:
      - "reto_2/Dockerfile.nginx"
      - "reto_2/nginx_templates/.*"
      - "reto_2/99-auth_basic_ssl_certs.sh"
  stage: build
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG: ${CI_REGISTRY_IMAGE}/proxy_reverso:latest
  script:
    - echo $IMAGE_TAG  $CI_REGISTRY_IMAGE
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG -f reto_2/Dockerfile.nginx reto_2/
    - docker push $IMAGE_TAG

build_helm:
  image: vfarcic/helm
  rules:
    - changes:
      - "reto_5/proxy-reverso-retodevops/*"
      - "reto_5/proxy-reverso-retodevops/charts/*"
      - "reto_5/proxy-reverso-retodevops/templates/tests/*"
      - "reto_5/proxy-reverso-retodevops/templates/*"
  stage: build
  script:
    - helm package ./reto_5/proxy-reverso-retodevops
  artifacts:
    expire_in: 1 day
    paths:
      - proxy-reverso-retodevops-*.tgz
```

## Release
Esta etapa tiene 2 tareas: **build_app_release** y **release_helm**:
  - **build_app_release**: Esta tarea construye una imagen de la app nodejs y es publicada con tag latest. Solo se gatilla cuando se hace un merge request con rama de destino master, de modo que solo queden con ese tag builds estables.
  - **release_helm**: Esta tarea publica en el repositorio de paquetes el helm chart. Solo se gatilla cuando se realizan cambios en el helm chart (directorio `reto_5/proxy-reverso-retodevops`).

```yaml
release_helm:
  rules:
    - changes:
      - "reto_5/proxy-reverso-retodevops/*"
      - "reto_5/proxy-reverso-retodevops/charts/*"
      - "reto_5/proxy-reverso-retodevops/templates/tests/*"
      - "reto_5/proxy-reverso-retodevops/templates/*"
  stage: release
  script:
    - HELM_CHART_VERSION=$(ls | grep tgz | cut -d'-' -f4 | sed 's/.tgz//')
    - HELM_CHART_PACKAGE=$(ls | grep tgz)
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${HELM_CHART_PACKAGE} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/reto-devops/${HELM_CHART_VERSION}/${HELM_CHART_PACKAGE}"'

build_app_release:
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH && $CI_MERGE_REQUEST_APPROVED && $CI_PIPELINE_SOURCE == "merge_request_event"'
  cache:
    paths:
      - node_modules
  image: docker:19.03.12
  stage: release
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG_APP: $CI_REGISTRY_IMAGE:latest
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG_APP .
    - docker push $IMAGE_TAG_APP
```
