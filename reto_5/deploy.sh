#!/bin/bash

minikube addons enable ingress
minikube addons enable helm-tiller
helm repo add prx-rev https://gitlab.com/FerNandoSed/reto-devops/-/raw/retofco/reto_5/proxy-reverso-retodevops/
helm repo update
# Reintenta varias veces la instalación.
helm install prx-reverso prx-rev/proxy-reverso-retodevops --namespace=retodevops --set secretPasswd=abc123,secretUser=admin --atomic --wait --debug || \ 
    helm install prx-reverso prx-rev/proxy-reverso-retodevops --namespace=retodevops --set secretPasswd=abc123,secretUser=admin --atomic --wait --debug || \
    helm install prx-reverso prx-rev/proxy-reverso-retodevops --namespace=retodevops --set secretPasswd=abc123,secretUser=admin --atomic --wait --debug || \
    helm install prx-reverso prx-rev/proxy-reverso-retodevops --namespace=retodevops --set secretPasswd=abc123,secretUser=admin --atomic --wait --debug