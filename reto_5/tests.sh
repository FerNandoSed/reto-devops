#!/bin/bash

set -e

MINIKUBEIP=$(minikube ip)
curl http://${MINIKUBEIP} -H "Host: retodevops.local" -i
curl https://${MINIKUBEIP} -H "Host: retodevops.local" -ik
curl https://${MINIKUBEIP}/public -H "Host: retodevops.local" -ik
curl https://${MINIKUBEIP}/private -H "Host: retodevops.local" -ik
curl https://${MINIKUBEIP}/private -H "Host: retodevops.local" -ik -uadmin:abc123

