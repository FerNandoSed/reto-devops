# Reto 5
El Helm chart diseñado está compuesto de un deployment que utiliza la imagen de proxy reverso preparada anteriormente y un ingress que implementa el enpoint TLS. Dado que se utilizó ingress para TLS, el proxy reverso tuvo que ser configurado para HTTP plano, por lo que se recurrió a un configmap montado en el directorio de templates para nginx.

## Procedimiento de Ejecución

### Requisitos

  * El despliegue del [reto_4](/reto_4/README.md) debe estar corriendo.
  * Para poder utilizar *ingress*, se debe agregar la línea `$(minikube ip) retodevops.local` al archivo hosts de la estación de trabajo. Ej:

    ```bash
    ❯ minikube ip
    192.168.99.106

    ❯ sudo vi /etc/hosts
    ...
    (Agregar línea '192.168.99.106 retodevops.local')
    ...
    ```
  * Se debe habilitar addon ingress.
    ```bash
    # Habilitar ingress
    ❯ minikube addons enable ingress
    🔎  Verifying ingress addon...
    🌟  The 'ingress' addon is enabled

    ...
    ```
  * En caso de utilizar Helm 2.x, habilitar helm-tiller addon.
    ```bash
    ❯ minikube addons enable helm-tiller
    🌟  The 'helm-tiller' addon is enabled
    ```
### Instalación de Helm Chart
Posteriormente se instala el helm chart.

```bash
❯ helm repo add prx-rev https://gitlab.com/FerNandoSed/reto-devops/-/raw/retofco/reto_5/proxy-reverso-retodevops/
"prx-rev" has been added to your repositories

❯ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "prx-rev" chart repository...

❯ helm install prx-reverso prx-rev/proxy-reverso-retodevops --namespace=retodevops --set secretPasswd=abc123,secretUser=admin
W1106 22:12:51.951866   56167 warnings.go:67] spec.template.metadata.annotations[security.alpha.kubernetes.io/unsafe-sysctls]: non-functional in v1.11+; use the "sysctls" field instead
NAME: prx-reverso
LAST DEPLOYED: Sat Nov  6 22:12:51 2021
NAMESPACE: retodevops
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  https://retodevops.local/

```
Nótese que hay que agregar los valores de `secretPasswd` y `secretUser` vía línea de comandos. Esto también se puede hacer con un archivo YAML con el siguiente contenido:

```yaml
secretPasswd: abc123
secretUser: admin
```

y ejecutando la siguiente sentencia, donde `secreto.yml` es el nombre del archivo yaml:

```bash
❯ helm install prx-reverso ./proxy-reverso-retodevops --debug --namespace=retodevops --values=secreto.yml
```

## Resultados

```bash
# Redirección hacia HTTPS
❯ curl http://retodevops.local -i
HTTP/1.1 308 Permanent Redirect
Date: Sun, 07 Nov 2021 01:17:05 GMT
Content-Type: text/html
Content-Length: 164
Connection: keep-alive
Location: https://retodevops.local

<html>
<head><title>308 Permanent Redirect</title></head>
<body>
<center><h1>308 Permanent Redirect</h1></center>
<hr><center>nginx</center>
</body>
</html>

# Bloqueo de endpoin /private
❯ curl https://retodevops.local/private -ik
HTTP/2 401 
date: Sun, 07 Nov 2021 01:14:39 GMT
content-type: text/html
content-length: 172
www-authenticate: Basic realm="Acceso Privado"

<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx</center>
</body>
</html>
❯ curl https://retodevops.local/private -ik -uadmin:abc123
HTTP/2 200 
date: Sun, 07 Nov 2021 01:14:46 GMT
content-type: application/json; charset=utf-8
content-length: 60
etag: W/"3c-cRwqN1Rv9e5tabqBVi4dqsdQZ1o"

{"private_token":"TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="}

# Funcionamiento normal
❯ curl https://retodevops.local/public -ik -uadmin:abc123
HTTP/2 200 
date: Sun, 07 Nov 2021 01:16:32 GMT
content-type: application/json; charset=utf-8
content-length: 42
etag: W/"2a-7p86Ecm2J1NsJJNY7yYZtbUuw9s"

{"public_token":"12837asd98a7sasd97a9sd7"}
```