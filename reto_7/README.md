# Reto 7: Makefile

El Makefile creado ejecutará el despliegue de cada reto y hará pruebas utilizando curl y kubectl, de modo de probar que los servicios funcionan. El reto 3 no tiene pruebas y en este caso solo se gatilla una ejecución del pipeline.
## Consideraciones

El Makefile hará uso de los siguientes puertos en la estación de trabajo donde se ejecute:

  * **reto_1**: 127.0.0.1:3000
  * **reto_2**: 127.0.0.1:80, 127.0.0.1:443
  * **reto_4**: 127.0.0.1:3004
  * **reto_5**: $(minikube ip):80, $(minikube ip):443

Además se forzará la utilización del contexto kubetctl minikube, de modo de hacer el despliegue en el cluster local manejado por minikube.

## Ejecución

En el directorio raíz del repositorio clonado, ejecutar `make`
```bash
make
```

Output:
```bash
echo "Ejecutando Reto 1"
Ejecutando Reto 1
docker build . --tag app-retodevops
Sending build context to Docker daemon  12.29kB
Step 1/12 : FROM alpine:3.14.2
 ---> 14119a10abf4
Step 2/12 : ENV DEPENDENCIES "npm nodejs"
 ---> Using cache
 ---> 1ad7280752e6
Step 3/12 : RUN apk add --update ${DEPENDENCIES}
 ---> Using cache
 ---> 33b4278e8dc0
Step 4/12 : ENV USER "retodevops"
 ---> Using cache
 ---> cb29f3c24fae
Step 5/12 : RUN addgroup ${USER} &&     adduser -h /retodevops/ -D -G ${USER} ${USER}
 ---> Using cache
 ---> 0ae8f4c4e933
Step 6/12 : COPY ./ /retodevops/
 ---> Using cache
 ---> 346d00971778
Step 7/12 : RUN cd /retodevops &&     npm install &&     chown -R ${USER}:${USER} /retodevops
...
...
```

Una ejecución exitosa debería mostrar el siguiente error cuado finaliza:

```bash
kubectl delete deployment retodevops --context=auditor-context
Error from server (Forbidden): deployments.apps "retodevops" is forbidden: User "auditor" cannot delete resource "deployments" in API group "apps" in the namespace "retodevops"
make: [Makefile:16: test_reto6] Error 1 (ignored)
```

Este error indica que el usuario creado en el reto 6 no puede borrar deployments, lo cual es esperable debido a que solo puede revisar pods.

Se puede detener los procesos en ejecución con la siguiente sentencia:
```bash
make clean -ik
```

## Errores recurrentes

### failed calling webhook "validate.nginx.ingress.kubernetes.io"

El error se manifiesta con el siguiente mensaje:
```
Error: release prx-reverso failed, and has been uninstalled due to atomic being set: Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": Post "https://ingress-nginx-controller-admission.ingress-nginx.svc:443/networking/v1/ingresses?timeout=10s": context deadline exceeded
helm.go:81: [debug] Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": Post "https://ingress-nginx-controller-admission.ingress-nginx.svc:443/networking/v1/ingresses?timeout=10s": context deadline exceeded
```

Esto ocurre porque la API de ingress podría no estar lista al mometo de instalar el chart Helm del reto 5. El problema ocurre generalmente cuando el reto 5 es ejecutado inmediátamente después del reto 4. El workaround en repetir la setencia `make` para retomar el despliegue del reto 5.
