# Reto 4: Kubernetes
Para realizar este reto se utilizó minikube. Para desplegar la aplicación se utilizó la imagen desarrollada en el reto 1 y publicada en el registro de contenedores del repositorio, lo cual fue logrado en el reto 3.

## Procedimiento de ejecución

### Despliegue de aplicación

```bash
## Inicia minikube
❯ minikube start --driver=virtualbox
😄  minikube v1.16.0 on Ubuntu 20.04
✨  Using the virtualbox driver based on user configuration
👍  Starting control plane node minikube in cluster minikube
🔥  Creating virtualbox VM (CPUs=2, Memory=2200MB, Disk=20000MB) ...
🐳  Preparing Kubernetes v1.20.0 on Docker 20.10.0 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default


# Habilita el servidor de métricas
❯ minikube addons enable metrics-server
🌟  The 'metrics-server' addon is enabled

# Crea namespace
❯ kubectl create namespace retodevops
namespace/retodevops created

# Ir a directorio con manifests y desplegar infraestructura
❯ cd reto_4/k8s_manifests
❯ kubectl apply -f hpa.yml -f service.yml -f deployment.yml --namespace=retodevops
horizontalpodautoscaler.autoscaling/retodevops created
service/retodevops created
deployment.apps/retodevops created
```

Para probar el acceso al sericio se utiliza redirección de puertos
```bash
❯ kubectl --namespace retodevops port-forward $(kubectl get pods --namespace=retodevops | grep retodevops |cut -f1 -d' ') 3000:3000
Forwarding from 127.0.0.1:3000 -> 3000
Forwarding from [::1]:3000 -> 3000
Handling connection for 3000

# En otra ventana probar con curl
❯ curl localhost:3000
{"msg":"ApiRest prueba"}
❯ curl localhost:3000/private
{"private_token":"TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="}
```

### Forzando el escalamiento de pods horizontal

Para forzar el escalamiento de la aplicación, el umbral de uso de CPU señalado en el *HorizontalPodAutoscaler* (**HPA**) fue limitado a un 2%, por lo que casi cualquier tipo de carga lo puede hacer crecer. Esto se hizo con el fin de generar un ejemplo de funcionamiento.

A continuación muestro algunas imágenes de lo conseguido durante las pruebas:

**Ejecución de Apache Benchmark**
Para generar la carga se utilizó Apache Benchmark. Durante las pruebas se había utilizado servicetype NodePort, por lo que esto difiere de la documentación más arriba. De todos modos, si se ocupa port forwarding la línea de ejecución sería la siguiente:
```bash
❯ ab -n1000 -t 180 http://localhost:3000/public
```

![Ejecución de ab](./img/ab.png)

**Deployments**
En esta imagen se puede ver como crece el número de pods mientras se genera la carga.

![deployments](./img/deployments.png)

**HPA**
Acá se muestra como varia el uso de CPU y cuando es que comienzan a aumentar los pods.

![HPA](./img/hpa.png)
