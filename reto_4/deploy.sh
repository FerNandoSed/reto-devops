#!/bin/bash

LOCALPORT=3004

function start_minikube() {
    minikube start --driver=virtualbox
    minikube addons enable metrics-server
}

function app_ready(){
    while :
    do
        echo -n > /dev/tcp/localhost/$LOCALPORT
        APPISREADY=$?

        if [[ $APPISREADY -eq 1 ]]; then
            sleep 1
            echo app not ready yet...
        else
            echo app ready!!!
            break
        fi
    done
}

function deployment() {
    kubectl create namespace retodevops
    kubectl apply -f reto_4/k8s_manifests/hpa.yml -f reto_4/k8s_manifests/service.yml -f reto_4/k8s_manifests/deployment.yml --namespace=retodevops
    while :
    do
        kubectl get pod -n retodevops | grep Running && break || sleep 10
    done
    (kubectl --namespace retodevops port-forward service/retodevops $LOCALPORT:3000 & ) && app_ready
}

minikube status || start_minikube
if [ $(kubectl config current-context) = "minikube" ]; then
    deployment
else
    echo configurando contexto minikube
    kubectl config use-context minikube
    deployment
fi
