#!/bin/bash

set -e

WORKDIR=rbac
export USERNAME=${1:-auditor}
NAMESPACE=retodevops

[ -d ${WORKDIR} ] || mkdir -p ${WORKDIR}

if [ ! -f "${WORKDIR}/signing-request.yml" ]; then
    echo Creando CSR usuario ${USERNAME}
    openssl genrsa -out ${WORKDIR}/${USERNAME}.key 2048
    openssl req -new -key ${WORKDIR}/${USERNAME}.key -out ${WORKDIR}/${USERNAME}.csr -subj "/CN=${USERNAME}/O=retodevops"
    echo Creando signing request
    export CSR=$(cat ${WORKDIR}/${USERNAME}.csr | base64 | tr -d '\n','%')
    envsubst < ${WORKDIR}/signing-request.yml.tmpl > ${WORKDIR}/signing-request.yml
fi

kubectl create -f ${WORKDIR}/signing-request.yml

kubectl certificate approve ${USERNAME}-csr

kubectl get csr ${USERNAME}-csr -o jsonpath='{.status.certificate}' | base64 --decode > ${WORKDIR}/${USERNAME}.crt

kubectl config set-credentials ${USERNAME} --client-certificate=${WORKDIR}/${USERNAME}.crt --client-key=${WORKDIR}/${USERNAME}.key

kubectl config set-context ${USERNAME}-context --cluster=minikube --namespace=${NAMESPACE} --user=${USERNAME}

rm "${WORKDIR}/signing-request.yml"
