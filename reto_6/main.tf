module "rbac_auditor" {
  source = "./modules/rbac_auditor"
  namespace = "retodevops"
  user = "auditor"
  rolename = "retodevops_reader"
  rolebindingname = "retodevops_reader"
}