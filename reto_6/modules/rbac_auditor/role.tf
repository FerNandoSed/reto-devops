resource "kubernetes_role" "role" {
  metadata {
    name = "${var.rolename}"
    namespace = "${var.namespace}"
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}