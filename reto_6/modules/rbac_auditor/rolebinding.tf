resource "kubernetes_role_binding" "role_binding" {
  metadata {
    name      = "${var.rolebindingname}"
    namespace = "${var.namespace}"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${var.rolename}"
  }
  subject {
    kind      = "User"
    name      = "${var.user}"
    api_group = "rbac.authorization.k8s.io"
  }
}