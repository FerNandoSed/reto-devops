# Reto 6: Terraform
El módulo construido implementa un Role y Rolebinding que limitan las acciones de un usuario dado sobre un namespace en particular.
## Procedimiento de Ejecución

Primero se debe crear un usuario, el cual será asociado al rol.
```bash
# navegar al directorio reto_6
cd reto_6/

# Ejecutar creación de usuario con nombre "auditor"
❯ ./generate_user.sh auditor
Creando CSR usuario auditor
Generating RSA private key, 2048 bit long modulus (2 primes)
............................................................................................+++++
...................................................+++++
e is 65537 (0x010001)
Creando signing request
certificatesigningrequest.certificates.k8s.io/auditor-csr created
certificatesigningrequest.certificates.k8s.io/auditor-csr approved
User "auditor" set.
Context "auditor-context" modified.


```

Para Crear el rol y asociar el usuario, se aplica plan de terraform.
```bash
# Inicialización de módulos
❯ terraform init
+Initializing modules...

Initializing the backend...
 
Initializing provider plugins...
- Reusing previous version of hashicorp/kubernetes from the dependency lock file
- Using previously-installed hashicorp/kubernetes v2.4.1

Terraform has been successfully initialized!
...

# Aplicación de plan
❯ terraform apply
module.rbac_auditor.kubernetes_role_binding.role_binding: Refreshing state... [id=retodevops/retodevops_reader]
module.rbac_auditor.kubernetes_role.role: Refreshing state... [id=retodevops/retodevops_reader]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the
last "terraform apply":

  # module.rbac_auditor.kubernetes_role_binding.role_binding has been changed
  ~ resource "kubernetes_role_binding" "role_binding" {
        id = "retodevops/retodevops_reader"
...
```
Una vez aplicado el plan, puedo listar el *role* y *rolebinding* creados.
```bash
❯ kubectl get roles -n retodevops
NAME                CREATED AT
retodevops_reader   2021-08-30T03:09:04Z

❯ kubectl get rolebindings -n retodevops
NAME                ROLE                     AGE
retodevops_reader   Role/retodevops_reader   6m45s
```

## Pruebas

Si intento borrar un deployment con el contexto de usuario auditor, obtendré un error por falta de permisos.
```bash
❯ kubectl delete deployment retodevops --context=auditor-context
Error from server (Forbidden): deployments.apps "retodevops" is forbidden: User "auditor" cannot delete resource "deployments" in API group "apps" in the namespace "retodevops"
```
Tampoco puedo borrar pods.
```bash
❯ kubectl delete pods retodevops-78fb956849-2lthp --context=auditor-context
Error from server (Forbidden): pods "retodevops-78fb956849-2lthp" is forbidden: User "auditor" cannot delete resource "pods" in API group "" in the namespace "retodevops"
```
En cambio, puedo observar los pods en el namespace *retodevops*.
```bash
❯ kubectl get pods --context=auditor-context -w
NAME                                                      READY   STATUS    RESTARTS   AGE
prx-retodeops-proxy-reverso-retodevops-78d859d74b-prjtm   1/1     Running   0          30m
retodevops-78fb956849-2lthp                               1/1     Running   0          117m

```