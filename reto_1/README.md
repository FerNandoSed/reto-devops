# Reto 1
Para obtener una imagen lo más pequeña posible se ha utilizado alpine, con lo que se obtuvo 127MB. Para ejecutar la aplicación se creó el usuario retodevops, y la aplicación con sus dependencias quedan en el directorio /retodevops.

## Construir y ejecutar la aplicación

Para construir la imagen, se debe ejecutar la siguiente línea.
```bash
# El build context corresponde a la raíz del repositorio.
docker build . --tag app-retodevops
```

Para correr la aplicación:
```bash
docker run --name retodevops --rm --init -p3000:3000 app-retodevops
```

## Resultados:

![Reto_1](./reto_1.gif)