# Se fija la version de alpine para controlar la descarga de imagenes
FROM alpine:3.14.2

# Dependencias de la imagen
ENV DEPENDENCIES "npm nodejs"

# Instala dependencias
RUN apk add --update ${DEPENDENCIES}

# Agrega usuario con el que va a correr la aplicacion
ENV USER "retodevops"
RUN addgroup ${USER} && \
    adduser -h /retodevops/ -D -G ${USER} ${USER}

# Agrega aplicacion
COPY ./ /retodevops/

# Instala dependencias nodejs de la app
RUN cd /retodevops && \
    npm install && \
    chown -R ${USER}:${USER} /retodevops
# Elimina paquetes innecesarios.
RUN apk del npm brotli-libs c-ares libretls nghttp2-libs 

EXPOSE 3000/tcp
USER ${USER}:${USER}
WORKDIR /retodevops
CMD ["node","index.js"]
