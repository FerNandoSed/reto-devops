# Reto 2
A continuación se describe lo realizado para cumplir los requisitos de reto 2.

## Configuración proxy reverso NGINx

Se parte con la imagen nginx:stable-alpine como base, la cual tiene la ventaja de poder agregar configuraciones a través de plantillas y además se pueden agregar scripts custom al entrypoint, aunque esto requiere de la reconstrucción de la imagen.

Se utilizó un script custom para generar un certificado autofirmado (para HTTPS) y el archivo de autentificación básica. El contenido de este archivo se puede completar a partir de variables de ambientes cargadas en el archivo `reto_2/.env_example`.

### Contenido archivo de variables de ambiente

```bash
NGINX_CERTIFICATE_FILE_PATH=/etc/nginx/cert.pem
NGINX_PRIVATE_KEY_FILE_PATH=/etc/nginx/privatekey.pem
NGINX_HT_PASSWD=passwordmuys3cr3t0
NGINX_HT_USER=admin
```

## Configuración de la app

Se utiliza el Dockerfile del reto anterior para generar la imagen que será utilizada como contenedor. No se exponen los puertos, con tal de que la aplicación solo sea accedida desde el proxy reverso.

## Levantamiento del stack

Para levantar el stack se debe crear un archivo con las variables de ambiente que estará situado en `reto_2/.env`, el cual puede completarse a partir de `reto_2/.env_example`.

```bash
# Copia archivo .env de ejemplo
cp reto_2/.env_example reto_2/.env

# Cambia contenido de .env
vi reto_2/.env

# Ejecuta stack
docker-compose up
Starting reto-devops_app_1 ... done
Starting reto-devops_web_1 ... done
Attaching to reto-devops_web_1, reto-devops_app_1
web_1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
web_1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
web_1  | 10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
app_1  | Example app listening on port 3000!
web_1  | 10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf differs from the packaged version
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
web_1  | 20-envsubst-on-templates.sh: Running envsubst on /etc/nginx/templates/0-ssl.conf.template to /etc/nginx/conf.d/0-ssl.conf
web_1  | 20-envsubst-on-templates.sh: Running envsubst on /etc/nginx/templates/retodevops.conf.template to /etc/nginx/conf.d/retodevops.conf
web_1  | 20-envsubst-on-templates.sh: Running envsubst on /etc/nginx/templates/default.conf.template to /etc/nginx/conf.d/default.conf
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
web_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/99-auth_basic_ssl_certs.sh
web_1  | Archivo para auth_basic existe.....
web_1  | Certificado existe.....
...
```

## Resultados
### Redirección hacia HTTPS

![redir_https](./img/redireccion_https.png)

![HTTPS habilitado](./img/HTTPS.png)

Se agrega flag `-k` ya que el certificado es autofirmado.

### Requerimiento de autorización en endpoint /private
![Denegación private endpoint](./img/private_endpoint_denegado.png)

![Private endpoint autorizado](./img/private_endpoint_autorizado.png)