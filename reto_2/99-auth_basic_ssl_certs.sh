#!/bin/sh
set -e

function create_cert() {
    apk add openssl
    openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/C=CL/ST=LL/L=Inexistente/O=CLM/CN=retodevops.xyz" \
        -keyout ${NGINX_PRIVATE_KEY_FILE_PATH}  -out ${NGINX_CERTIFICATE_FILE_PATH}
    apk del openssl
}

# Crea archivo de auth_basic si es que no existe todavía
if [ ! -f /etc/nginx/.htpasswd ]; then
    echo Creando archivo para auth basic...
    # Instala htpasswd para generar archivo de autentificacion basica
    apk add apache2-utils

    # genera auth basic
    htpasswd -cb /etc/nginx/.htpasswd ${NGINX_HT_USER} ${NGINX_HT_PASSWD}

    # Remueve el paquete para evitar utilizar espacio
    apk del apache2-utils
else
    echo Archivo para auth_basic existe.....
fi

# Crea certificado autofirmado, si es que no existe
if [ ! -f "${NGINX_CERTIFICATE_FILE_PATH}" ]; then
    if [ -f "${NGINX_PRIVATE_KEY_FILE_PATH}" ]; then
        rm "${NGINX_PRIVATE_KEY_FILE_PATH}"
    fi
    echo Creando certificado.....
    create_cert
else
    echo Certificado existe.....
fi